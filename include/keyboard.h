#ifndef KEYBOARD_H
#define KEYBOARD_H

/* Defines */
#define KEYBOARD_DATA_PORT 0x60
#define KEYBOARD_STATUS_PORT 0x64
#define ENTER_KEY_CODE 0x1C

/* Init keyboard */
void kb_init(void);

#endif
