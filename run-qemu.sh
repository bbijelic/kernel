#!/bin/bash

# Directory of a script
directory=$(dirname $0)

# Source defines
source $directory/defines.h

# Run kernel in Qemu for i386
qemu-system-i386 -kernel $directory/$build_dir/$kernel_filename-$kernel_version

