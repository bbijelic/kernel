#include "keyboard.h"
#include "extern.h"
#include "display.h"

char *vidptr = (char*)0xb8000;

/* Current location */
unsigned int text_row = 21;
unsigned int default_text_column = 5;
unsigned int text_column = 5;

void kb_init(void) {
	/* 0xFD is 11111101 - enables only IRQ1 (keyboard)*/
	write_port(0x21 , 0xFD);
}

void keyboard_handler_main(void){
	unsigned char status;
	char keycode;

	/* write EOI */
	write_port(0x20, 0x20);

    /* Read status of the port */
	status = read_port(KEYBOARD_STATUS_PORT);

    /* Lowest bit of status will be set if buffer is not empty */
	if (status & 0x01) {

        /* Read port and obtain keycode */
		keycode = read_port(KEYBOARD_DATA_PORT);
		if(keycode < 0) return;

		if(keycode == ENTER_KEY_CODE) {
            text_row++;
            text_column = default_text_column;
			return;

		} else if (keycode == 0x0E){
            /* Make sure that backspace does not delete */
            /* before the default column */
            if(text_column - 1 >= default_text_column  ){
                text_column--;
                display_write_one(' ', text_row, text_column);
            }
            return;
        }

        /* Write text to display */
		display_write_one(keyboard_map[(unsigned char)keycode], text_row, text_column);
        text_column++;
	}
}
