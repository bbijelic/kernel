#!/bin/bash

# Directory of a script
directory=$(dirname $0)

# Source defines
source $directory/defines.h

if [[ $1 == "--clean" ]]
then
	echo "Cleaning '$build_dir' directory ..."
	rm -vrf $directory/$build_dir/*
fi

# Assemble
nasm -f elf32 $directory/$kernel_assembly -o $directory/$build_dir/kasm.o
echo "Assembled $kernel_assembly ..."

# Compile kernel
gcc -fno-stack-protector -m32 -I $directory/$include_dir -c $directory/kernel.c -o $directory/$build_dir/kc.o
gcc -fno-stack-protector -m32 -I $directory/$include_dir -c $directory/display.c -o $directory/$build_dir/display.o
gcc -fno-stack-protector -m32 -I $directory/$include_dir -c $directory/idt.c -o $directory/$build_dir/idt.o
gcc -fno-stack-protector -m32 -I $directory/$include_dir -c $directory/keyboard.c -o $directory/$build_dir/keyboard.o
echo "Compiled kernel source ..."

# Link
ld -m elf_i386 -T $directory/$link_file -o $directory/$build_dir/$kernel_filename-$kernel_version $directory/$build_dir/kasm.o $directory/$build_dir/kc.o $directory/$build_dir/display.o $directory/$build_dir/idt.o $directory/$build_dir/keyboard.o
echo "Linked kernel to $build_dir/$kernel_filename-$kernel_version ..."

