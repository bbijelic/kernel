#!/bin/bash

# Directories
include_dir=include
build_dir=build

# Kernel
kernel_version='0.1'
kernel_filename='kernel'

# Assembly kernel
kernel_assembly=kernel.asm

# Link description file
link_file=link.ld

