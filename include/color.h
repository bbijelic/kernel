#ifndef COLOR_H
#define COLOR_H

/* Define color custom type */
typedef unsigned char color_t;

#define BLACK		0x00
#define BLUE		0x01
#define GREEN		0x02
#define CYAN		0x03
#define RED         0x04
#define MAGENTA		0x05
#define BROWN		0x06
#define LGRAY		0x07
#define	DGRAY		0x08
#define LBLUE		0x09
#define LGREEN		0x0A
#define LCYAN		0x0B
#define LRED		0x0C
#define LMAGENTA	0x0D
#define LBROWN		0x0E
#define WHITE		0x0F

#endif
