#include "display.h"

/* Display foreground color */
color_t display_foreground_color = BLACK;

/* Display background color */
color_t display_background_color = WHITE;

/* Display memory address pointer */
char* display_mem_ptr = (char*) VIDEO_MEMORY_ADDRESS;

/* Prints string on a screen starting at a gien line and column */
void display_write(char* text, unsigned int row, unsigned int column){
    unsigned int index = ( row * VIDEO_COLUMNS + column ) * VIDEO_WORD_WIDTH;
    for(unsigned int i = 0; text[i] != '\0' ; i++){
        display_mem_ptr[index] = text[i];
        display_mem_ptr[index+1] = ((display_background_color << 4) & 0xF0 ) | display_foreground_color;
        index += VIDEO_WORD_WIDTH;
    }
}

/* Write signle character on screen starting at a given line and column */
void display_write_one(unsigned char ch, unsigned int row, unsigned int column){
    unsigned int index = ( row * VIDEO_COLUMNS + column ) * VIDEO_WORD_WIDTH;
    display_mem_ptr[index] = ch;
    display_mem_ptr[index+1] = ((display_background_color << 4) & 0xF0 ) | display_foreground_color;
}

/* Cleans text from the display */
void display_clear(){
    unsigned int index = 0;
    while( index < VIDEO_MEMORY_BUFFER_SIZE ) {
		/* Write blank character */
		display_mem_ptr[index] = 0x00;
		/* attribute-byte */
		display_mem_ptr[index+1] = display_background_color << 4;
		index += VIDEO_WORD_WIDTH;
	}
}

/* Sets foreground color */
void display_set_fg_color(color_t color){
    display_foreground_color = color;
}

/* Sets background_color */
void display_set_bg_color(color_t color) {
    display_background_color = color;
}
