#ifndef EXTERN_H
#define EXTERN_H

/* Port read and write */
extern char read_port(unsigned short port);
extern void write_port(unsigned short port, unsigned char data);

/* Keyboard handler */
extern void keyboard_handler(void);
extern unsigned char keyboard_map[128];

/* Disables cursor */
/* Defined in kernel.asm */
extern void display_disable_cursor(void);

/* Load IDT */
extern void load_idt(unsigned long *idt_ptr);

#endif
