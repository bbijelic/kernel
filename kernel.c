#include "display.h"
#include "idt.h"
#include "keyboard_map.h"
#include "keyboard.h"

/* Kernel main function */
void kmain(void) {

	/* Disable cursor */
	/* Defined in kernel.asm */
	display_disable_cursor();

	/* Initialize display */
	display_set_bg_color(WHITE);
	display_set_fg_color(BLACK);
	display_clear();

	unsigned int rose_column = 6;

	display_set_fg_color(LGRAY);

	display_write("    .o oOOOOOOOo                                            OOOo", 1, rose_column);
	display_write("    Ob.OOOOOOOo  OOOo.      oOOo.                      .adOOOOOOO", 2, rose_column);
	display_write("    OboO\"\"\"\"\"\"\"\"\"\"\"\".OOo. .oOOOOOo.    OOOo.oOOOOOo..\"\"\"\"\"\"\"\"\"\'OO", 3, rose_column);
	display_write("    OOP.oOOOOOOOOOOO \"POOOOOOOOOOOo.   `\"OOOOOOOOOP,OOOOOOOOOOOB\'", 4, rose_column);
	display_write("    `O\'OOOO'     `OOOOo\"OOOOOOOOOOO` .adOOOOOOOOO\"oOOO\'    `OOOOo", 5, rose_column);
	display_write("    .OOOO'            `OOOOOOOOOOOOOOOOOOOOOOOOOO\'            `OO", 6, rose_column);
	display_write("    OOOOO                 \'\"OOOOOOOOOOOOOOOO\"`                oOO", 7, rose_column);
	display_write("   oOOOOOba.                .adOOOOOOOOOOba               .adOOOOo.", 8, rose_column);
	display_write("  oOOOOOOOOOOOOOba.    .adOOOOOOOOOO@^OOOOOOOba.     .adOOOOOOOOOOOO", 9, rose_column);
	display_write(" OOOOOOOOOOOOOOOOO.OOOOOOOOOOOOOO\"`  \'\"OOOOOOOOOOOOO.OOOOOOOOOOOOOO", 10, rose_column);
	display_write(" \"OOOO\"       \"YOoOOOOMOIONODOO\"`  .   '\"OOROAOPOEOOOoOY\"     \"OOO\"", 11, rose_column);
	display_write("    Y           \'OOOOOOOOOOOOOO: .oOOo. :OOOOOOOOOOO?\'         :`", 12, rose_column);
	display_write("    :            .oO%OOOOOOOOOOo.OOOOOO.oOOOOOOOOOOOO?         .", 13, rose_column);
	display_write("    .            oOOP\"%OOOOOOOOoOOOOOOO?oOOOOO?OOOO\"OOo", 14, rose_column);
	display_write("                 \'%o  OOOO\"%OOOO%\"%OOOOO\"OOOOOO\"OOO\':", 15, rose_column);
	display_write("                      `$\"  `OOOO\' `O\"Y ' `OOOO\'  o             .", 16, rose_column);
	display_write("    .                  .     OP\"          : o     .", 17, rose_column);
	display_write("                              :", 18, rose_column);
	display_write("                              .", 19, rose_column);

	/* Display kernel name */
	display_set_fg_color(BLACK);
	display_write("MicroKernel v0.0.1", 19, 2);

	/* Keyboard Interrup handling */
	display_set_fg_color(DGRAY);
	display_write("Keyboard Interrupt Handler Demo:", 20, 2);

	/* Keyboard input */
	display_set_fg_color(BLACK);
	display_write(">>", 21, 2);
	display_set_fg_color(RED);	

	idt_init();
	kb_init();

	while(1);

	return;
}
