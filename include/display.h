#ifndef DISPLAY_H
#define DISPLAY_H

#include "color.h"
#include "extern.h"

/* Video memory */
#define VIDEO_MEMORY_ADDRESS 0xb8000
#define VIDEO_ROWS 25
#define VIDEO_COLUMNS 80
#define VIDEO_WORD_WIDTH 2
#define VIDEO_MEMORY_BUFFER_SIZE VIDEO_ROWS * VIDEO_COLUMNS * VIDEO_WORD_WIDTH

/* Prints string on a screen starting at a gien line and column */
void display_write(char* str, unsigned int row, unsigned int column);

/* Write signle character on screen starting at a given line and column */
void display_write_one(unsigned char ch, unsigned int row, unsigned int column);

/* Cleans text from the display */
void display_clear();

/* Sets foreground color */
void display_set_fg_color(color_t color);

/* Sets background_color */
void display_set_bg_color(color_t color);

#endif
